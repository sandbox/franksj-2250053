var f;
var url;
(jQuery)(document).ready(function () {
	f = (jQuery)('iframe');
  if (f && f.attr('src')) {
	  url = f.attr('src').split('?')[0];
	  (jQuery)('iframe[src*="vimeo"]').not('[src*="api=1"]').each(function (index) { (jQuery)(this).attr("src", (jQuery)(this).attr("src").replace("?", "?api=1&")); });

	  // Listen for messages from the player
	  if (window.addEventListener){
			  window.addEventListener('message', onMessageReceived, false);
	  }
	  else {
			  window.attachEvent('onmessage', onMessageReceived, false);
	  }
  }
});

// Handle messages received from the player
function onMessageReceived(e) {
    var data = JSON.parse(e.data);
    
    switch (data.event) {
        case 'ready':
            onReady();
            break;
           
        case 'playProgress':
            onPlayProgress(data.data);
            break;
            
        case 'pause':
            onPause();
            break;
           
        case 'finish':
            onFinish();
            break;
    }
}

// Helper function for sending a message to the player
function post(action, value) {
    var data = { method: action };
    
    if (value) {
        data.value = value;
    }
    
    if (url.indexOf('http') == -1) {
      var pro = window.location.protocol;
      url = pro + url;
    }

    f[0].contentWindow.postMessage(JSON.stringify(data), url);
}

function onReady() {
    (jQuery)('.vimeo-player-status').text('ready');
    
    post('addEventListener', 'pause');
    post('addEventListener', 'finish');
    post('addEventListener', 'playProgress');
}

function onPause() {
    (jQuery)('.vimeo-player-status').text('paused');
}

function onFinish() {
    (jQuery)('.vimeo-player-status').text('finished');
    vimeo_api_callback('finished');
}

function onPlayProgress(data) {
    (jQuery)('.vimeo-player-status').text(data.seconds + 's played');
}

function vimeo_api_callback(eventName) {
  var postUrl = Drupal.settings.vimeo_api_url;
  (jQuery).get(postUrl, function(data) { vimeo_api_callback_success(data); });
}

function vimeo_api_callback_success(data) {

}